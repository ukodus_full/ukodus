# Ukodus project

Ukodus is a reverse game of sudoku, where the player tries to set the board for a sudoku as difficult as possible
and then the machine has the task of solving it as quickly as possible.

The main goal of this project is to provide the libraries implementing the necessary abstractions and algorithms,
but for the sake of completeness (and testing) there are also two source files with main functions, which are
built as the binaries `ukodus` and `string_ukodus` in the `bin` folder.

## Generality:

The libraries are designed to be very flexible and support many variations of sudoku. The board doesn't have to
be `9x9`. It can be `nxn` for any `n > 0`. The blocks (if used) are then as square as possible. For example, in a
`6x6` game, the blocks are `2x3`. But even the blocks are not a fixed aspect of the game. Instead, a "constraint" is
simply any partition of the `n^2` tiles into `n` parts of exactly `n` tiles each. The rules then say that every
"value" can appear at most once in each "part" of this partition, and the game is "solved" if all constraints are
satisfied and all tiles are set (i.e. contain some "value"). Using rows, columns and blocks to define constraints is
just one possibility, but the user can define arbitrary constraints.

Limitations on how large `n` can be come from the fact that we use `char` to represent a "value" in a tile.
Nevertheless, as sudoku is an NP-hard problem, instances that cannot be encoded using `char` are probably too large
to be solved by a computer anyway.

## The libraries:

### `utils`

Here you'll find some helpful classes and methods that have nothing to do with the game itself, and could just
as well be in an external library. For example, `UT::PrettyTime` is a wrapper around `std::chrono::duration<>` which
helps print duration values in a nice, human readable format, with units and all.

### `base`

This subproject defines the abstractions that constitute the game (e.g. `BS::Board`, `BS::Constraint` and
`BS::Instance`). The board holds a simple array of entry values and is unaware of rows, columns and blocks. This
has to do with the generality of the implementation, as described above. For constraints we have the three classic
ones (`BS::RowConstraint`, `BS::ColumnConstraint` and `BS::BlockConstraint`) but also the `BS::ArbitraryConstraint`,
where the user can set the partition of the tiles anyway they want (as long as all parts contain the same number of
tiles). In an instance, the constraints are fixed, and only the board can be manipulated. For gradually adding
constraints one has to use `BS::InstanceParms::register_constraint` (prior to constructing the instance).

### `algorithms`

The class `AL::Algorithm` is the base for all solver implementations. Algorithms return (a) a solution (by modifying
the values on the instance's board), (b) the time it took to finish and (c) a return code. The return code says either
(1) that a solution was found, or (2) that no solution exists (and the algorithm has proven it), or (3) that the
algorithm gave up (different implementations might give up for different reasons such as number of iterations or time)
without solving and without proving that there is no solution, or (4) that the initial state of the instance was
illegal.

Currently there's only one implementation available: `AL::BruteForceAlgorithm`. This solver enumerates all possible
solutions and is guaranteed to always find one, if one exists. It is not as fast as some other algorithms, but
still takes only a few milliseconds for most puzzles. Some specially designed puzzles can be the death of
`AL::BruteForceAlgorithm` (see e.g. `instances/unfeasible02.uko`), so it has an iteration counter and gives up
after a maximum number of iterations (defined on construction).

The struct `AL::Algorithms` is a sort of factory for different algorithm implementations and offers a "default"
method `AL::Algorithms::default_try_solve` which uses a hard-coded default implementation.

### `interface`

This subproject contains some helper classes for parsing text and building instances out of it. They are used in the
two executables from this project and in the iOS GUI from the sibling project `iOS GUI` (also inside the `Ukodus`
group). For a description of the encoding used in the text-input files, check out `instances/template.uko` or any
of the other files in `instances` whose name doesn't end with `_string.uko`.

## The executables:

### `ukodus`

This program takes a text file as input (suggested extension is `.uko`) encoded as described in
`instances/template.uko` and prints out the results to the standard output stream, including (1) the initial
state of the board (in a pretty grid), (2) whether the initial state is valid, (3) the return code of the
algorithm call, (4) the time it took to solve and (5) the solved board (again in a pretty grid). Usage is

	ukodus <my_instance_text_file>

### `string_ukodus`

This program is mainly designed for testing. It works similarly to `ukodus` but takes as input a single
string with one character for each tile of the board. It's ugly to use. One example call would be

	string_ukodus _4_68__7______71__28______57________5__2_1__3________40______31__21______7__60_2_
