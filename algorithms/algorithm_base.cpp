#include "algorithm_base.hpp"

#include <base/instance.hpp>

#include <stdexcept>

namespace AL
{

	Algorithm::Algorithm (BS::Instance & instance_)
	:
		_instance(instance_),
		_solver(instance())
	{}


	Algorithm::RetValue
	Algorithm::try_solve () const
	{
		RetValue ret;

		if (not instance().state_is_valid())
		{
			ret.code = TrySolveRetCode::initial_state_illegal;
			ret.time = TimeType(0);
		}
		else if (instance().is_solved())
		{
			ret.code = TrySolveRetCode::solved;
			ret.time = TimeType(0);
		}
		else
		{
			auto const before = std::chrono::high_resolution_clock::now();
			ret.code = try_solve_impl();
			auto const after = std::chrono::high_resolution_clock::now();

			ret.time = std::chrono::duration_cast<TimeType>(after - before);
		}

		return ret;
	}

	BS::Instance &
	Algorithm::instance () const
	{
		return _instance;
	}

	BS::Solver const &
	Algorithm::solver () const
	{
		return _solver;
	}

	void
	output (UT::OStream & out, Algorithm::TrySolveRetCode tsrc)
	{
		switch (tsrc)
		{
			case Algorithm::TrySolveRetCode::initial_state_illegal:
				out << "initial state illegal"; break;
			case Algorithm::TrySolveRetCode::solved:
				out << "solved"; break;
			case Algorithm::TrySolveRetCode::proved_unfeasibility:
				out << "proved unfeasibility"; break;
			case Algorithm::TrySolveRetCode::gave_up:
				out << "gave up"; break;
			default:
				throw std::invalid_argument("Invalid Algorithm::TrySolveRetCode output to stream.");
		}
	}

} // namespace AL
