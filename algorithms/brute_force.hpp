#ifndef ALGORITHMS_BRUTE_FORCE_HPP
#define ALGORITHMS_BRUTE_FORCE_HPP

#include <algorithms/algorithm_base.hpp>

namespace AL
{

	class BruteForceAlgorithm final
	: public Algorithm
	{
	public:

		BruteForceAlgorithm (BS::Instance & instance_, unsigned max_num_iterations_);

	private:
		TrySolveRetCode
		try_solve_impl () const override;

		unsigned
		max_num_iterations () const;

		unsigned const _max_num_iterations;
	};

} // namespace AL

#endif /* ALGORITHMS_BRUTE_FORCE_HPP */
