#ifndef ALGORITHMS_DANCING_LINKS_HPP
#define ALGORITHMS_DANCING_LINKS_HPP

#include <algorithms/algorithm_base.hpp>

namespace AL
{

	class DancingLinksAlgorithm final
	: public Algorithm
	{
	public:

		DancingLinksAlgorithm (BS::Instance & instance_);

	private:
		TrySolveRetCode
		try_solve_impl () const override;
	};

} // namespace AL

#endif /* ALGORITHMS_DANCING_LINKS_HPP */
