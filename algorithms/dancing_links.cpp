#include "dancing_links.hpp"

#include <base/instance.hpp>

namespace AL
{

	DancingLinksAlgorithm::DancingLinksAlgorithm (BS::Instance & instance_)
	: Algorithm(instance_) {}

	Algorithm::TrySolveRetCode
	DancingLinksAlgorithm::try_solve_impl () const
	{
		assert(instance().is_solved());
		return TrySolveRetCode::solved;

		// TODO
	}

} // namespace AL
