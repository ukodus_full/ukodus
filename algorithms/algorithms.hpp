#ifndef ALGORITHMS_ALGORITHMS_HPP
#define ALGORITHMS_ALGORITHMS_HPP

#include <algorithms/algorithm_base.hpp>

namespace AL
{

	/**
		@struct Algorithms

		Similar to an Algorithm factory, but since Algorithm only has
		one public method, this struct constructs an Algorithm and calls
		this method straight away, without returning the constructed
		Algorithm to the caller.
	*/
	struct Algorithms final
	{

		using RetValue = Algorithm::RetValue;

		static
		RetValue
		default_try_solve (BS::Instance & instance);

		static
		RetValue
		brute_force_try_solve (BS::Instance & instance);

	};

} // namespace AL

#endif /* ALGORITHMS_ALGORITHMS_HPP */
