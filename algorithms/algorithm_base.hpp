#ifndef ALGORITHMS_ALGORITHM_BASE_HPP
#define ALGORITHMS_ALGORITHM_BASE_HPP

#include <base/modifier.hpp>

#include <chrono>

namespace BS
{
	class Instance;
}

namespace AL
{

	/**
		@class Algorithm

		Base class for different implementations of sudoku solvers.
	*/
	class Algorithm
	{
	public:

		/**
			@enum TrySolveRetCode

			A special return-code type for the try_solve method.

			Algorithm implementations may solve an instance or prove that it is unfeasible,
			both of which are considered a success, since the Algorithm performed its job
			correctly. They are also allowed to give up if the instance is too hard (e.g. after
			a long time, or after a large number of attempted solutions etc). Parameters for
			giving up conditions may be set e.g. on construction of the particular impl object.
		*/
		enum class TrySolveRetCode
		{
			initial_state_illegal,
			solved,
			proved_unfeasibility,
			gave_up
		};

		using
		TimeType = std::chrono::nanoseconds;

		struct RetValue
		{
			TrySolveRetCode code;
			TimeType time;
		};

		Algorithm (BS::Instance & instance_);

		virtual
		~Algorithm () = default;

		RetValue
		try_solve () const;

	protected:
		BS::Instance &
		instance () const;

		BS::Solver const &
		solver () const;
	private:
		virtual
		TrySolveRetCode
		try_solve_impl () const = 0;

		BS::Instance & _instance;
		BS::Solver _solver;
	};

	void
	output (UT::OStream & out, Algorithm::TrySolveRetCode tsrc);

} // namespace AL

#endif /* ALGORITHMS_ALGORITHM_BASE_HPP */
