#include "brute_force.hpp"

#include <base/instance.hpp>
#include <base/constraint_check.hpp>

#include <stack>

namespace AL
{

	BruteForceAlgorithm::BruteForceAlgorithm (BS::Instance & instance_, unsigned max_num_iterations_)
	:
		Algorithm(instance_),
		_max_num_iterations(max_num_iterations_)
	{}

	unsigned
	BruteForceAlgorithm::max_num_iterations () const
	{
		return _max_num_iterations;
	}

	Algorithm::TrySolveRetCode
	BruteForceAlgorithm::try_solve_impl () const
	{
		BS::ColorUseRange color_use_range(instance());
		BS::RawEntryValue const num_allowed_values = instance().as_board().num_allowed_values();

		/***************************************************************************************************
		 * Part 0: Define a bunch of useful lambdas.
		 */

		auto is_legal = [&color_use_range] (BS::EntryValue value, BS::EntryIndex idx) -> bool
		{
			bool is_used_in_at_least_one = false;

			for (auto & color_use : color_use_range)
			{
				is_used_in_at_least_one = (is_used_in_at_least_one or color_use.is_used(value, idx));
			}

			return not is_used_in_at_least_one;
		};

		// Unsafe, but should be okay as a local type within this method.
		struct IndexWithCandidates
		{
			using CandidateStack = std::stack<BS::EntryValue, std::vector<BS::EntryValue>>;

			IndexWithCandidates (BS::EntryIndex idx_)
			: idx(idx_), candidates() {}

			BS::EntryIndex idx;
			CandidateStack candidates;
		};

		auto recompute_candidates = [num_allowed_values, &is_legal] (IndexWithCandidates & iwc)
		{
			assert(iwc.candidates.empty());

			for (BS::RawEntryValue raw_value = 0; raw_value < num_allowed_values; ++raw_value)
			{
				BS::EntryValue const value = BS::entry_from(raw_value);
				if (is_legal(value, iwc.idx))
				{
					iwc.candidates.push(value);
				}
			}
		};

		auto change_value = [this, &color_use_range] (BS::EntryIndex idx, BS::EntryValue new_value)
		{
			BS::EntryValue const old_value = instance().as_board().entry(idx).value;

			if (old_value != BS::EntryValue::unset)
			{
				for (auto & color_use : color_use_range) { color_use.is_used(old_value, idx) = false; }
			}

			solver().set_value(idx, new_value);

			if (new_value != BS::EntryValue::unset)
			{
				for (auto & color_use : color_use_range) { color_use.is_used(new_value, idx) = true; }
			}
		};

		/************************************************************************************************
		 * Part 1: Collect all indices of entries we're intend to modify. These are precisely the unset
		 * entries. We don't want to use Modifier::is_allowed_to_modify because then if we concatenated
		 * different algorithms, they would overwrite each other's progress (since solver can overwrite
		 * solver).
		 */

		auto collect_unset = [this] () -> std::vector<IndexWithCandidates>
		{
			std::vector<IndexWithCandidates> ret;

			BS::EntryIndex const num_entries = instance().as_board().num_entries();
			for (BS::EntryIndex idx = 0; idx < num_entries; ++idx)
			{
				if (instance().as_board().entry(idx).value == BS::EntryValue::unset) { ret.push_back(idx); }
			}

			return ret;
		};

		auto modifiables = collect_unset();

		/**************************************************************************************************
		 * Part 2: Pseudo-recursively try all possibilities
		 */

		recompute_candidates(modifiables.front());
		auto iwc_it = modifiables.begin();

		for (unsigned count = 0; count < max_num_iterations(); ++count)
		{
			if (iwc_it->candidates.empty())
			{
				if (iwc_it == modifiables.begin()) // There is no solution!
				{
					return TrySolveRetCode::proved_unfeasibility;
				}

				// There are no solutions with the current values of the previous modifiables.
				// Unset the value for the current entry:
				change_value(iwc_it->idx, BS::EntryValue::unset);
				// Go back to the previous modifiable and move on to its next candidate:
				--iwc_it;
			}
			else
			{
				// Move on to the next candidate:
				change_value(iwc_it->idx, iwc_it->candidates.top());
				iwc_it->candidates.pop();
				// Move on to the next modifiable entry:
				++iwc_it;

				if (iwc_it == modifiables.end())
				{
					assert(instance().is_solved());
					return TrySolveRetCode::solved;
				}

				recompute_candidates(*iwc_it);
			}
		}

		return TrySolveRetCode::gave_up;
	}

} // namespace AL
