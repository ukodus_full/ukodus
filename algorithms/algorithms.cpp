#include "algorithms.hpp"

#include <algorithms/brute_force.hpp>

namespace AL
{

	Algorithms::RetValue
	Algorithms::brute_force_try_solve (BS::Instance & instance)
	{
		unsigned const max_num_iterations = 1 << 28; // 2^28 ~ 270 million
		return BruteForceAlgorithm(instance, max_num_iterations).try_solve();
	}

	Algorithms::RetValue
	Algorithms::default_try_solve (BS::Instance & instance)
	{
		return brute_force_try_solve(instance);
	}

} // namespace AL
