#ifndef UTILS_GRID_PRINTER_HPP
#define UTILS_GRID_PRINTER_HPP

#include <utils/stream_wrapper.hpp>

namespace UT
{

	class GridPrinter final
	{
	public:

		GridPrinter (std::size_t max_elements_per_row_)
		: _max_elements_per_row(max_elements_per_row_) {}

		template <typename InputIt>
		void
		print (UT::OStream & out, InputIt begin, InputIt end) const;

	private:
		std::size_t
		max_elements_per_row () const
		{
			return _max_elements_per_row;
		}

		std::size_t const _max_elements_per_row;
	};



	//BEGIN: Template implementation
	template <typename InputIt>
	void
	GridPrinter::print (UT::OStream & out, InputIt begin, InputIt end) const
	{
		auto print_separating_line = [this, &out] ()
		{
			for (std::size_t i = 0; i < max_elements_per_row(); ++i) { out << "+---"; }
			out << '+' << std::endl;
		};

		auto print_single_row = [this, &out, &begin, &end] ()
		{
			std::size_t column = 0;

			for (; (begin != end) and (column < max_elements_per_row()); ++begin, ++column)
			{
				out << "| " << (*begin) << ' ';
			}

			for (; column < max_elements_per_row(); ++column)
			{
				out << "|   ";
			}

			out << '|' << std::endl;
		};

		while (begin != end)
		{
			print_separating_line();
			print_single_row();
		}

		print_separating_line();
	}
	//END: Template implementation

} // namespace UT

#endif /* UTILS_GRID_PRINTER_HPP */
