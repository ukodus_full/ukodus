#ifndef UTILS_STREAM_WRAPPER_HPP
#define UTILS_STREAM_WRAPPER_HPP

#include <ostream>

namespace UT
{

	/**
		@class OStream

		This simple wrapper around an @c std::ostream allows us to redefine the output operator's
		behaviour by overloading our own global method @c output, without overloading or specializing
		anything in the @c std namespace, which can often cause headaches.
	*/
	class OStream final
	{
	public:

		OStream (std::ostream & inner_)
		: _inner(inner_) {}

		template <typename T>
		OStream &
		operator<< (T const & t)
		{
			output(*this, t);
			return *this;
		}

		/** Special overload for @c std::ostream manipulators */
		OStream &
      operator<< (std::ostream & (* func_ptr) (std::ostream &))
      {
			func_ptr(inner());
			return *this;
      }

		std::ostream &
		inner ()
		{
			return _inner;
		}

	private:
		std::ostream & _inner;
	};

	/**
		Default version of @c output which should be called whenever we do not overload it for the type
		@c T passed as second argument. Simply forwards @c t to the output operator of the internal
		@c std::ostream.
	*/
	template <typename T> inline
	void
	output (OStream & out, T const & t)
	{
		out.inner() << t;
	}

	/** Global static wrapper for std::cout */
	OStream &
	coutw ();

	/** Global static wrapper for std::cerr */
	OStream &
	cerrw ();

} // namespace UT

#endif /* UTILS_STREAM_WRAPPER_HPP */
