#include "time_printer.hpp"

#include <utils/retcode.hpp>

#include <cassert>
#include <array>

namespace UT
{

	void
	output (OStream & out, PrettyTime const & time)
	{
		enum UnitRank
		{
			ur_first_unit,
			ur_second_unit
		};

		///////////////////////////////////////////////////////////////////////////////////////
		// Helpful lambdas: (A bit repetitive, but it would take some work to be able to
		// traverse the different time units and associate them with the different behaviours
		// and strings and so on.)

		auto print_hours = [&out, &time] () -> RetCode
		{
			auto const value = std::chrono::duration_cast<std::chrono::hours>(time.base_time).count();

			if (value != 0)
			{
				out << value;

				if (value == 1) { out << " hour"; }
				else            { out << " hours"; }
			}

			return success_if(value != 0);
		};

		auto print_minutes = [&out, &time] (UnitRank ur) -> RetCode
		{
			auto const value = std::chrono::duration_cast<std::chrono::minutes>(
				time.base_time - std::chrono::duration_cast<std::chrono::hours>(time.base_time)).count();

			if (value != 0)
			{
				if (ur == ur_second_unit) { out << " and "; }

				out << value;

				if (value == 1) { out << " minute"; }
				else            { out << " minutes"; }
			}

			return success_if(value != 0);
		};

		auto print_seconds = [&out, &time] (UnitRank ur) -> RetCode
		{
			auto const value = std::chrono::duration_cast<std::chrono::seconds>(
				time.base_time - std::chrono::duration_cast<std::chrono::minutes>(time.base_time)).count();

			if (value != 0)
			{
				if (ur == ur_second_unit) { out << " and "; }

				out << value;

				if (value == 1) { out << " second"; }
				else            { out << " seconds"; }
			}

			return success_if(value != 0);
		};

		auto print_milliseconds = [&out, &time] (UnitRank ur) -> RetCode
		{
			auto const value = std::chrono::duration_cast<std::chrono::milliseconds>(
				time.base_time - std::chrono::duration_cast<std::chrono::seconds>(time.base_time)).count();

			if (value != 0)
			{
				if (ur == ur_second_unit) { out << " and "; }

				out << value;

				if (value == 1) { out << " millisecond"; }
				else            { out << " milliseconds"; }
			}

			return success_if(value != 0);
		};

		auto print_microseconds = [&out, &time] () -> RetCode
		{
			auto const value = std::chrono::duration_cast<std::chrono::microseconds>(
				time.base_time - std::chrono::duration_cast<std::chrono::milliseconds>(time.base_time)).count();

			if (value != 0)
			{
				out << value;

				if (value == 1) { out << " microsecond"; }
				else            { out << " microseconds"; }
			}

			return success_if(value != 0);
		};

		auto print_nanoseconds = [&out, &time] () -> RetCode
		{
			auto const value = std::chrono::duration_cast<std::chrono::nanoseconds>(
				time.base_time - std::chrono::duration_cast<std::chrono::microseconds>(time.base_time)).count();

			if (value != 0)
			{
				out << value;

				if (value == 1) { out << " nanosecond"; }
				else            { out << " nanoseconds"; }
			}

			return success_if(value != 0);
		};

		////////////////////////////////////////////////
		// Tricky conditionals:

		if (print_hours() == RetCode::success)
		{
			print_minutes(ur_second_unit);
		}
		else if (print_minutes(ur_first_unit) == RetCode::success)
		{
			print_seconds(ur_second_unit);
		}
		else if (print_seconds(ur_first_unit) == RetCode::success)
		{
			print_milliseconds(ur_second_unit);
		}
		else if (print_milliseconds(ur_first_unit) == RetCode::success)
		{
			/* NOP */
		}
		else if (print_microseconds() == RetCode::success)
		{
			/* NOP */
		}
		else if (print_nanoseconds() == RetCode::success)
		{
			/* NOP */
		}
		else
		{
			out << "no time at all";
		}
	}

} // namespace UT
