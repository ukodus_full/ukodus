#ifndef UTILS_RETCODE_HPP
#define UTILS_RETCODE_HPP

#include <utils/stream_wrapper.hpp>

#include <stdexcept>

namespace UT
{

	enum class RetCode
	{
		success,
		failure
	};

	inline
	RetCode
	success_if (bool condition)
	{
		return condition ? RetCode::success : RetCode::failure;
	}

	inline
	void
	output (OStream & out, RetCode rc)
	{
		switch (rc)
		{
			case RetCode::success:
				out << "success"; break;
			case RetCode::failure:
				out << "failure"; break;
			default:
				throw std::invalid_argument("Unknown return code output to stream.");
		}
	}

} // namespace UT

#endif /* UTILS_RETCODE_HPP */
