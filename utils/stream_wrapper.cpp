#include "stream_wrapper.hpp"

#include <iostream>

namespace UT
{

	OStream &
	coutw ()
	{
		static OStream ret(std::cout);
		return ret;
	}

	OStream &
	cerrw ()
	{
		static OStream ret(std::cerr);
		return ret;
	}

} // namespace UT
