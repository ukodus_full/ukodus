#ifndef UTILS_TIME_PRINTER_HPP
#define UTILS_TIME_PRINTER_HPP

#include <utils/stream_wrapper.hpp>
#include <chrono>

namespace UT
{

	struct PrettyTime final
	{
		using
		TimeType = std::chrono::nanoseconds;

		template <typename Rep, typename Period>
		PrettyTime (std::chrono::duration<Rep, Period> base_time_)
		:
			base_time(std::chrono::duration_cast<TimeType>(base_time_))
		{}

		friend
		void
		output (OStream & out, PrettyTime const & time);

		TimeType const base_time;
	};

} // namespace UT

#endif /* UTILS_TIME_PRINTER_HPP */
