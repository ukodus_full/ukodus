#include "string_helpers.hpp"

#include <algorithm>

namespace UT
{

	std::string &
	StringHelp::remove_characters (std::string & str, std::initializer_list<char> list)
	{
		std::string::iterator new_end = std::remove_if(str.begin(), str.end(),
			[&list] (char c)
			{
				return (std::find(list.begin(), list.end(), c) != list.end());
			}
		);

		str.erase(new_end, str.end());
		return str;
	}

} // namespace UT
