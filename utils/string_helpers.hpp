#ifndef UTILS_STRING_HELPERS_HPP
#define UTILS_STRING_HELPERS_HPP

#include <string>
#include <initializer_list>

namespace UT
{

	struct StringHelp final
	{

		static
		std::string &
		remove_characters (std::string & str, std::initializer_list<char> list);

	};

} // namespace UT

#endif /* UTILS_STRING_HELPERS_HPP */
