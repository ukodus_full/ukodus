### Include helper makefiles
include Makefile.config
include Makefile.local

# Make sure the documentation files directory exists
$(shell mkdir -p $(DOC_DIR) >/dev/null)

# Make sure the dependency files directory exists
$(shell mkdir -p $(DEP_DIR) >/dev/null)

# Make sure the binary files directory exists
$(shell mkdir -p $(BIN_DIR) >/dev/null)

# Rules
.PHONY: default all binary doc clean cleanall cleanbinary cleandoc cleandep $(SUBPROJS) $(MAIN_PROJ)

default: binary
	@echo "***** Running a quick test: *****"
	@$(BIN_DIR)/string_ukodus $(shell cat $(PROJ_DIR)/instances/evil01_string.uko)
	@echo "***** Finished test. *****"
# 	@$(BIN_DIR)/ukodus $(PROJ_DIR)/instances/tom6x6.uko

all: binary doc

binary: $(SUBPROJS) $(MAIN_PROJ)
	@$(MAKE) -C $(MAIN_PROJ) link

$(SUBPROJS) $(MAIN_PROJ):
	@$(MAKE) -C $@ compile

doc:
	@echo "Doxygen not set up yet"

clean: cleanbinary cleandep

cleanall: cleanbinary cleandoc cleandep

cleanbinary:
	@echo "Removing binary files"
	@$(RM) $(BIN_DIR)/*

cleandoc:
	@echo "Removing documentation files"
	@$(RM) $(DOC_DIR)/*

cleandep:
	@echo "Removing dependency files"
	@$(RM) $(DEP_DIR)/*
