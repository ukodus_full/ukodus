#ifndef INTERFACE_TEXT_SETTER_HPP
#define INTERFACE_TEXT_SETTER_HPP

#include <base/modifier.hpp>

namespace IF
{

	class TextSetter final
	{
	public:

		TextSetter (BS::Instance & instance_);

		// Note: Intentional copy. This idiom facilitates moving if a temporary is passed.
		/**
			@throws std::invalid_argument if the string has the wrong format.

			The expected format is as follows:
			- Start with '{', finish with '}'.
			- In between should be @c _num_allowed_values many relevant characters, separated by commata
			  (which are not relevant).
			- Whitespace can be used at will, meaning spaces and tabs (but not newlines).
			- Relevant characters are '0', '1', ... , '9', 'a', 'b', ... , 'z' and '_', where the underscore
			  represents an unset entry.

			Example with @c _num_allowed_values == 9:
				<tt>"\t{a, c, t, _, _\t,    5, r, _, 0}  "</tt>
		*/
		void
		set_next_row_from (std::string str);

		bool
		end_of_grid () const;

	private:
		BS::Setter const &
		inner_setter () const;

		BS::Setter _inner_setter;
		BS::RawEntryValue _cur_row;
		BS::RawEntryValue const _num_allowed_values;
	};

} // namespace IF

#endif /* INTERFACE_TEXT_SETTER_HPP */
