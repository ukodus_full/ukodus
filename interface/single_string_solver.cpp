#include "single_string_solver.hpp"

#include <base/instance.hpp>
#include <base/common_constraints.hpp>

#include <algorithms/algorithms.hpp>

#include <interface/text_setter.hpp>

#include <cmath>
#include <sstream>

namespace IF
{

	std::pair<AL::Algorithm::RetValue, std::string>
	StringSolver::parse_and_try_solve (std::string const & board_str)
	{
		BS::EntryIndex const num_entries = board_str.length();
		BS::RawEntryValue const num_allowed_values = std::sqrt(num_entries);

		assert(num_allowed_values * num_allowed_values == num_entries);

		// Create instance with traditional constraints:
		BS::Instance instance(
			BS::InstanceParms(num_allowed_values)
				.register_constraint(std::make_unique<BS::RowConstraint>(num_allowed_values))
				.register_constraint(std::make_unique<BS::ColumnConstraint>(num_allowed_values))
				.register_constraint(std::make_unique<BS::BlockConstraint>(num_allowed_values))
		);

		// Set the entries of the instance to the values given through board_str:
		TextSetter setter(instance);

		for (BS::RawEntryValue row = 0; row < num_allowed_values; ++row)
		{
			assert(not setter.end_of_grid());
			setter.set_next_row_from(board_str.substr(row * num_allowed_values, num_allowed_values));
		}

		assert(setter.end_of_grid());

		// Solve the instance using the default algorithm:
		auto const ret_val = AL::Algorithms::default_try_solve(instance);

		std::ostringstream oss;
		UT::OStream board_out(oss);

		for (BS::EntryIndex idx = 0; idx < num_entries; ++idx)
		{
			board_out << instance.as_board().entry(idx).value;
		}

		return std::make_pair(ret_val, oss.str());
	}

}
