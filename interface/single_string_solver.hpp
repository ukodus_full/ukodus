#ifndef INTERFACE_SINGLE_STRING_SOLVER_HPP
#define INTERFACE_SINGLE_STRING_SOLVER_HPP

#include <algorithms/algorithm_base.hpp>

namespace IF
{

	/**
		@class StringSolver

		This class is meant for easy bridging between C++ and Objective-C/Swift (used for the GUI).
		The idea is to skip over all the abstractions used in this project and get straight to
		business: Give me as input a string, where each character represents one entry of the board
		and I'll return a simple pair of integers representing a return code and the time it took me
		to solve the instance (in nanoseconds), plus a string representing the solution I found.
		As simple as it gets.
	*/
	struct StringSolver final
	{

		static
		std::pair<AL::Algorithm::RetValue, std::string>
		parse_and_try_solve (std::string const & board_str);

	};

}

#endif /* INTERFACE_SINGLE_STRING_SOLVER_HPP */
