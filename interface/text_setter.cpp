#include "text_setter.hpp"

#include <utils/string_helpers.hpp>

#include <stdexcept>

namespace IF
{

	TextSetter::TextSetter (BS::Instance & instance_)
	:
		_inner_setter(instance_),
		_cur_row(0),
		_num_allowed_values(instance_.as_board().num_allowed_values())
	{}

	namespace
	{
		// Unlike in the output method for EntryValue, the unset value here corresponds to
		// an underscore (instead of an empty space).
		BS::EntryValue
		char_to_entry (char c)
		{
			if (c == '_')
			{
				return BS::EntryValue::unset;
			}
			else if ((c >= '0') and (c <= '9'))
			{
				return BS::entry_from(c - '0');
			}
			else if ((c >= 'a') and (c <= 'z'))
			{
				return BS::entry_from(c - 'a' + 10); // 10 for the digits 0,1,...,9.
			}
			else
			{
				throw std::invalid_argument("Invalid character used to represent an EntryValue.");
			}
		}
	} // anonymous namespace

	void
	TextSetter::set_next_row_from (std::string str)
	{
		assert(not end_of_grid());

		// TODO: This method claims to check the string's format but only performs a
		// simplified partial check. A future version should look at the format in more detail.

		// Remove sugar coating
		UT::StringHelp::remove_characters(str, {' ', '\t', '{', '}', ','});

		if (str.length() != _num_allowed_values)
		{
			throw std::invalid_argument(
				"Given string has the wrong number of relevant characters or contains invalid characters.");
		}

		BS::EntryIndex const start = _cur_row * _num_allowed_values;

		for (std::size_t diff = 0; diff < str.length(); ++diff)
		{
			inner_setter().set_value(start + diff, char_to_entry(str[diff]));
		}

		++_cur_row;
	}

	bool
	TextSetter::end_of_grid () const
	{
		return _cur_row == _num_allowed_values;
	}

	BS::Setter const &
	TextSetter::inner_setter () const
	{
		return _inner_setter;
	}

} // namespace IF
