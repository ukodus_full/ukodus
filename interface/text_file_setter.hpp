#ifndef INTERFACE_TEXT_FILE_SETTER_HPP
#define INTERFACE_TEXT_FILE_SETTER_HPP

#include <base/instance.hpp>

#include <string>

namespace IF
{

	class TextFileParser final
	{
	public:

		/**
			@throws std::invalid_argument if the file cannot be opened or
			has the wrong format.
		*/
		static
		BS::Instance
		new_instance (std::string const & filename);

	};

} // namespace IF

#endif /* INTERFACE_TEXT_FILE_SETTER_HPP */
