#include "text_file_setter.hpp"

#include <utils/string_helpers.hpp>

#include <base/common_constraints.hpp>

#include <interface/text_setter.hpp>

#include <fstream>
#include <stdexcept>
#include <iostream>
#include <sstream>

namespace IF
{

	BS::Instance
	TextFileParser::new_instance (std::string const & filename)
	{
		// Open the text file:
		std::ifstream file_in(filename);

		// Check that the open was successful:
		if (not file_in.is_open())
		{
			throw std::invalid_argument(std::string("Could not open file: ") + filename);
		}

		// Lambda: Check that the line is neither a comment line nor an empty line.
		// Note: Expects the line to have already been stripped of empty spaces (i.e. spaces and tabs).
		auto is_relevant = [] (std::string const & line) -> bool
		{
			return (not line.empty()) and (line.front() != '#');
		};

		// Lambda: Like std::getline but only returns the relevant lines in the file (according to
		// previous lambda).
		auto smart_get_line = [&file_in, &is_relevant] (std::string & line) -> UT::RetCode
		{
			while (std::getline(file_in, line))
			{
				if (is_relevant(UT::StringHelp::remove_characters(line, {' ', '\t'})))
				{
					return UT::RetCode::success;
				}
			}

			return UT::RetCode::failure; // Failed to get line because there are no more lines.
		};

		std::string line;

		// Try to get the first line:
		UT::RetCode const first_rc = smart_get_line(line);

		// Check if there was an appropriate first line:
		if ((first_rc == UT::RetCode::failure) or (line.front() != '>'))
		{
			throw std::invalid_argument("File has the wrong format.");
		}

		// Extract value from first line (could be negative!):
		int const signed_num_allowed_values = std::stoi(line.substr(1));

		if (signed_num_allowed_values < 1)
		{
			std::ostringstream oss;
				oss << "Number of allowed values must be >= 1 (got: " << signed_num_allowed_values << ')';
			throw std::invalid_argument(oss.str());
		}

		// Set num_allowed_values.
		BS::RawEntryValue const num_allowed_values = signed_num_allowed_values;

		// Create instance with traditional constraints (i.e. row-, cloumn- and block-constraints):
		BS::Instance instance(
			BS::InstanceParms(num_allowed_values)
				.register_constraint(std::make_unique<BS::RowConstraint>(num_allowed_values))
				.register_constraint(std::make_unique<BS::ColumnConstraint>(num_allowed_values))
				.register_constraint(std::make_unique<BS::BlockConstraint>(num_allowed_values))
		);

		// Populate instance with values from the file:
		TextSetter setter(instance);

		while (not setter.end_of_grid())
		{
			UT::RetCode const rc = smart_get_line(line);

			if (rc == UT::RetCode::failure)
			{
				throw std::invalid_argument("File does not seem to contain enough lines to describe the instance.");
			}

			setter.set_next_row_from(line);
		}

		// Check that the file does not contain any further relevant lines. Extra lines would indicate a
		// probable mistake that could go unnoticed for a long time if these lines are simply ignored.
		// I.e., perhaps the second line (which was used) is 'too much' and should have been deleted, while
		// the last line (which would be ignored without the next check) is actually meant to be part of the
		// instance. This check prints a warning but does not throw. The program continues without taking
		// the extra lines into account.
		if (smart_get_line(line) == UT::RetCode::success)
		{
			UT::cerrw() << "[ukodus]: warning: File " << filename << " seems to contain more than "
				<< num_allowed_values << " 'instance'-lines." << std::endl;
		}

		return instance;
	}

} // namespace IF
