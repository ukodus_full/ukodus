#include "constraint.hpp"

#include <utils/grid_printer.hpp>

#include <vector>

namespace BS
{

	Constraint::Constraint (NumColorsType num_colors_)
	:
		_num_colors(num_colors_)
	{
		assert(num_colors() > 0);
	}

	Constraint::NumColorsType
	Constraint::num_colors () const
	{
		return _num_colors;
	}

	bool
	Constraint::is_valid () const
	{
		std::vector<NumColorsType> color_use_count(num_colors(), 0);

		// Check that there are no unset colors, and take the opportunity to count how
		// many times each color is used.
		for (EntryIndex idx = 0; idx < num_entries(); ++idx)
		{
			auto the_color = color(idx);

			if (the_color == EntryValue::unset) { return false; }
			++(color_use_count[value_of(the_color)]);
		}

		// Check that each color is used precisely num_colors() many times.
		for (auto num_uses : color_use_count)
		{
			if (num_uses != num_colors()) { return false; }
		}

		return true;
	}

	EntryIndex Constraint::num_entries () const
	{
		return num_colors() * num_colors();
	}

	namespace
	{
		class CountingLazyIterator final
		{
		public:

			using value_type = EntryValue;
			using reference = value_type;
			using self = CountingLazyIterator;

			CountingLazyIterator (Constraint const & constraint_, EntryIndex cur_)
			: _constraint(&constraint_), _cur(cur_) {}

			reference
			operator* () const
			{
				return _constraint->color(_cur);
			}

			self &
			operator++ ()
			{
				++_cur;
				return *this;
			}

			bool
			operator== (self const & other) const
			{
				return (_constraint == other._constraint) and (_cur == other._cur);
			}

			bool
			operator!= (self const & other) const
			{
				return not operator==(other);
			}

		private:
			Constraint const * _constraint;
			EntryIndex _cur;
		};

		CountingLazyIterator
		begin (Constraint const & constraint)
		{
			return CountingLazyIterator(constraint, 0);
		}

		CountingLazyIterator
		end (Constraint const & constraint)
		{
			return CountingLazyIterator(constraint, constraint.num_entries());
		}
	} // anonymous namespace

	void
	output (UT::OStream & out, Constraint const & constraint)
	{
		UT::GridPrinter(constraint.num_colors()).print(out, begin(constraint), end(constraint));
	}

} // namespace BS
