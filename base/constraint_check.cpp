#include "constraint_check.hpp"

#include <base/constraint.hpp>
#include <base/board.hpp>
#include <base/instance.hpp>

namespace BS
{

	ColorUse::ColorUse (Constraint const & constraint_)
	:
		_constraint(constraint_),
		_used(constraint().num_entries(), false)
	{}

	ColorUse::ColorUse (ColorUse && other)
	:
		_constraint(other._constraint),
		_used(std::move(other._used))
	{}

	ColorUse::bool_reference
	ColorUse::is_used (EntryValue value, EntryValue color)
	{
		assert(value_of(value) < constraint().num_colors());
		assert(value_of(color) < constraint().num_colors());

		EntryIndex const pos = (value_of(color) * constraint().num_colors()) + value_of(value);
		return _used[pos];
	}

	ColorUse::bool_reference
	ColorUse::is_used (EntryValue value, EntryIndex idx)
	{
		return is_used(value, constraint().color(idx));
	}

	UT::RetCode
	ColorUse::fill (Board const & board)
	{
		assert(board.num_allowed_values() == constraint().num_colors());

		for (EntryIndex idx = 0; idx < board.num_entries(); ++idx)
		{
			EntryValue const value = board.entry(idx).value;

			if (value == EntryValue::unset) { continue; }

			bool_reference value_used_in_color = is_used(value, idx);

			if (value_used_in_color)
			{
				return UT::RetCode::failure;
			}
			else
			{
				value_used_in_color = true;
			}
		}

		return UT::RetCode::success;
	}

	Constraint const &
	ColorUse::constraint () const
	{
		return _constraint;
	}

	ColorUseRange::ColorUseRange (Instance const & instance)
	:
		_color_uses()
	{
		UT::RetCode const rc = instance.for_each_constraint(
			[this, &instance] (BS::Constraint const & constraint) -> UT::RetCode
			{
				_color_uses.emplace_back(constraint);
				return _color_uses.back().fill(instance.as_board());
			}
		);

		assert(rc == UT::RetCode::success);
	}

	ColorUseRange::iterator
	ColorUseRange::begin ()
	{
		return _color_uses.begin();
	}

	ColorUseRange::iterator
	ColorUseRange::end ()
	{
		return _color_uses.end();
	}

} // namespace BS
