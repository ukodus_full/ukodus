#include "base_definitions.hpp"

#include <stdexcept>

namespace BS
{

	void
	output (UT::OStream & out, EntryValue value)
	{
		if (value == EntryValue::unset)
		{
			out << ' ';
		}
		else if (value_of(value) < 10) // print as decimal
		{
			out << value_of(value);
		}
		else if (value_of(value) - 10 < 26) // 26 letters in the alphabet
		{
			// After '9' continue from 'a' and until 'z'
			out << static_cast<char>('a' + value_of(value) - 10);
		}
		else // Refuse to print
		{
			// We only want to print 1-char values so that our grids look pretty
			throw std::invalid_argument("Value too high for printing with 1 character.");
		}
	}

	void
	output (UT::OStream & out, Entry entry)
	{
		out << entry.value;
	}

} // namespace BS
