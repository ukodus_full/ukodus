#ifndef BASE_BOARD_HPP
#define BASE_BOARD_HPP

#include <base/base_definitions.hpp>

#include <vector>

/**
	@file board.hpp

	Single-class header for @c BS::Board. The "board" represents the set of tiles in a sudoku
	game and the values written on them (or no value if a tile is "unset").

	@author <a href="mailto:tomasssalles@gmail.com">Tom</a>
*/

namespace BS
{
	//BEGIN: Forward-declarations.
	class Constraint;
	//END: Forward-declarations.

	/**
		@class Board

		Stores the state of the game (i.e. the values assigned to each tile) in a way that is
		intentionally constraint-unaware. In other words, the board does not know about rows,
		columns or blocks. It only knows that we can only use the values
		<tt>0, 1, ... , num_allowed_values() - 1</tt> (or the special value @c EntryValue::unset),
		and that it stores <tt>num_entries()</tt> of these (which happens to be
		<tt>num_allowed_values()</tt> squared).

		The idea is that constraints are implemented as arbitrary partitions of the set of tiles under
		the sole restriction that each "part" in the partition must contain precisely
		<tt>num_allowed_values()</tt> many tiles. Rows, columns and blocks are just three particular
		choices used in the traditional sudoku game, but this project supports arbitrary constraints.
	*/
	class Board final
	{
	public:

		/**
			@brief Only way to construct a new @c Board. Sets the number of allowed values and
			default-initializes all entries.

			@param num_allowed_values_ The number of values the player can use to set tiles. The
			valid values are then defined as <tt>0, 1, ... , num_allowed_values_ - 1</tt> plus the
			special value @c EntryValue::unset.

			@note After construction, the vector of @c Entry has size
			<tt>num_allowed_values_ * num_allowed_values_</tt> and this cannot be changed anymore.
		*/
		Board (RawEntryValue num_allowed_values_);

		/**
			@brief Move-constructor. Copies @c other._num_allowed_values (which is constant) and
			moves @c other._entries.

			@param other The @c Board to be moved into @c this.

			@note @c Board is neither copyable nor assignable, so this is the only way to return
			a @c Board from a function (without using smart-pointers).
		*/
		Board (Board && other);

		// Forbid copying Boards and assigning to them
		Board (Board const &) = delete;
		Board & operator= (Board const &) = delete;
		Board & operator= (Board &&) = delete;

		// This is here just to adhere to the rule of 5
		~Board () = default;

		/**
			@return The number of allowed values to be used by the player.

			In classic sudoku this is always 9.
		*/
		RawEntryValue
		num_allowed_values () const;

		/**
			@return The number of tiles on the board. Defined as the square of <tt>num_allowed_values()</tt>.

			In classic sudoku this is always 81.
		*/
		EntryIndex
		num_entries () const;

		/**
			@return The entry at index @c idx (read-only).

			@param idx The index of the desired entry in the underlying vector.

			The entry contains information about the current value of the corresponding tile, and who
			set that value (a "setter", a "player", a "solver" or no one).
		*/
		Entry const &
		entry (EntryIndex idx) const;

		/**
			@brief Sets the value of the tile of index @c idx to @c new_value. If the @c new_value is
			@c EntryValue::unset, the modifier of the entry is set to @c EntryModifier::none. Otherwise
			the entry's modifier is set to @c new_modifier.

			@param idx The index of the tile to be set.
			@param new_value The value to be written on the tile of index @c idx
			@param new_modifier The modifier who is writing the new value on the tile.

			@warning This method only performs the safety checks in debug mode (e.g. whether the index is
			within range, whether the new value is valid, that the new modifier is not @c none and that
			the new modifier has permission to change the value of the corresponding tile). These checks
			can take time and most algorithms have to perform them before trying to set the method anyway
			(for example, they can check permissions for @c new_modifier and @c idx only once and then
			set the value of the tile many times with different new values).
		*/
		void
		unchecked_set_value (EntryIndex idx, EntryValue new_value, EntryModifier new_modifier);

		/**
			@return Whether @c new_modifier is allowed to change values set by @c last_modifier.

			@param new_modifier A modifier.
			@param last_modifier Another modifier.

			An @c EntryModifier can be a @c setter, a @c player, a @c solver or @c none, and there
			are some rules about which modifiers can overwrite the choices of which other modifiers.
			For example, if a @c setter (who defines the initial state of the game) sets the value of
			a tile, then a @c player cannot change the value of that tile. But if a @c player sets
			the value of a tile, they can later change their minds and set the tile to a different
			value.
		*/
		static
		bool
		has_permission (EntryModifier new_modifier, EntryModifier last_modifier);

		/**
			@return Whether all tiles have some value other than @c EntryValue::unset written on them.
		*/
		bool
		all_entries_are_set () const;

		/**
			@return Whether @c this satisfies the given constraint in the current state.

			@param constraint The @c Constraint against which to check.
		*/
		bool
		satisfies (Constraint const & constraint) const;

	private:
		/**
			Output method so we can use <tt>out << board</tt> and get a pretty string representation
			of the @c Board. Uses @c UT::GridPrinter.

			(Declared in-class as a @c friend not only for access to private members but also to guarantee
			priority of this overload in name-lookup.)
		*/
		friend
		void
		output (UT::OStream & out, Board const & board);

		RawEntryValue const _num_allowed_values;
		std::vector<Entry> _entries;
	};

} // namespace BS

#endif /* BASE_BOARD_HPP */
