#include "arbitrary_constraint.hpp"

namespace BS
{

	ArbitraryConstraint::ArbitraryConstraint (NumColorsType num_colors_)
	:
		Constraint(num_colors_),
		_colors(num_entries(), EntryValue::unset)
	{}

	void
	ArbitraryConstraint::set_color (EntryIndex idx, EntryValue color)
	{
		assert(idx < _colors.size());
		assert(value_of(color) < num_colors());

		_colors[idx] = color;
	}

	EntryValue
	ArbitraryConstraint::color (EntryIndex idx) const
	{
		assert(idx < _colors.size());

		return _colors[idx];
	}

} // namespace BS
