#ifndef BASE_CONSTRAINT_CHECK_HPP
#define BASE_CONSTRAINT_CHECK_HPP

#include <utils/retcode.hpp>

#include <base/base_definitions.hpp>

#include <vector>

namespace BS
{
	class Constraint;
	class Board;
	class Instance;

	class ColorUse final
	{
	public:

		using bool_reference = std::vector<bool>::reference;

		ColorUse (Constraint const & constraint_);

		// Provide move constructor so ColorUse can be used in vectors
		ColorUse (ColorUse && other);

		// Forbid copying of ColorUse as it would be slow and should not be necessary
		ColorUse (ColorUse const &) = delete;

		// These are here just to adhere to the rule of 5
		~ColorUse () = default;
		ColorUse & operator= (ColorUse const &) = delete;
		ColorUse & operator= (ColorUse &&) = delete;

		/**
			@return A boolean-like reference whose value represents whether or not
			the value @c value is already used in the color @c color (as a part in
			the partition defined by the stored constraint).
		*/
		bool_reference
		is_used (EntryValue value, EntryValue color);

		/**
			Similar to <tt>is_used(EntryValue, EntryValue)</tt>, except the color is queried
			from the stored constraint.

			@note No ambiguity here because EntryValue is strongly typed.
		*/
		bool_reference
		is_used (EntryValue value, EntryIndex idx);

		/**
			@return @c success iff the constraint is satisfied by the board.
			@note If @c failure is returned the filling process was interrupted
			and the values stored in this object are meaningless.
		*/
		UT::RetCode
		fill (Board const & board);

	private:
		Constraint const &
		constraint () const;

		Constraint const & _constraint;
		std::vector<bool> _used;
	};

	class ColorUseRange final
	{
	public:

		using
		iterator = std::vector<ColorUse>::iterator;

		ColorUseRange (Instance const & instance);

		iterator
		begin ();

		iterator
		end ();

	private:
		std::vector<ColorUse> _color_uses;
	};

} // namespace BS

#endif /* BASE_CONSTRAINT_CHECK_HPP */
