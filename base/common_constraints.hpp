#ifndef BASE_COMMON_CONSTRAINTS_HPP
#define BASE_COMMON_CONSTRAINTS_HPP

#include <base/constraint.hpp>

namespace BS
{

	class RowConstraint final
	: public Constraint
	{
	public:

		RowConstraint (NumColorsType num_colors_);

		EntryValue
		color (EntryIndex idx) const override;

		/** @return @c true */
		bool
		is_valid () const override;

	};

	class ColumnConstraint final
	: public Constraint
	{
	public:

		ColumnConstraint (NumColorsType num_colors_);

		EntryValue
		color (EntryIndex idx) const override;

		/** @return @c true */
		bool
		is_valid () const override;

	};

	class BlockConstraint final
	: public Constraint
	{
	public:

		BlockConstraint (NumColorsType num_colors_);

		EntryValue
		color (EntryIndex idx) const override;

		/** @return @c true */
		bool
		is_valid () const override;

	private:
		using
		LengthType = NumColorsType;

		LengthType
		block_width () const;

		LengthType
		block_height () const;

		LengthType const _block_width;
		LengthType const _block_height;
	};

} // namespace BS

#endif /* BASE_COMMON_CONSTRAINTS_HPP */
