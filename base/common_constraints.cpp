#include "common_constraints.hpp"

#include <cstdlib>
#include <cmath>

namespace BS
{

	RowConstraint::RowConstraint (NumColorsType num_colors_)
	:
		Constraint(num_colors_)
	{}

	EntryValue
	RowConstraint::color (EntryIndex idx) const
	{
		return entry_from(idx / num_colors());
	}

	bool
	RowConstraint::is_valid () const
	{
		return true;
	}

	ColumnConstraint::ColumnConstraint (NumColorsType num_colors_)
	:
		Constraint(num_colors_)
	{}

	EntryValue
	ColumnConstraint::color (EntryIndex idx) const
	{
		return entry_from(idx % num_colors());
	}

	bool
	ColumnConstraint::is_valid () const
	{
		return true;
	}

	namespace
	{
		BlockConstraint::NumColorsType
		compute_block_width (BlockConstraint::NumColorsType num_colors)
		{
			assert(num_colors > 0);

			BlockConstraint::NumColorsType ret = std::round(std::sqrt(num_colors));

			// Set ret to the largest integer whose square is at most num_colors
			while (ret * ret > num_colors) { --ret; }

			// Set ret to the largest divisor of num_colors whose square is at most num_colors
			while ((num_colors % ret) != 0) { --ret; }

			return ret;
		}
	} // anonymous namespace

	BlockConstraint::BlockConstraint (NumColorsType num_colors_)
	:
		Constraint(num_colors_),
		_block_width(compute_block_width(num_colors())),
		_block_height(num_colors() / block_width())
	{
		// Check that no rounding was necessary to set the block height
		assert(block_width() * block_height() == num_colors());
	}

	EntryValue
	BlockConstraint::color (EntryIndex idx) const
	{
		std::div_t const divres = std::div(static_cast<unsigned>(idx), num_colors());
		EntryIndex const row = divres.quot;
		EntryIndex const column = divres.rem;

		EntryIndex const block_row = row / block_height();
		EntryIndex const block_column = column / block_width();

		EntryIndex const blocks_per_block_row = block_height(); // i.e. num_colors() / block_width()

		EntryIndex const block = (block_row * blocks_per_block_row) + block_column;
		return entry_from(block);
	}

	bool
	BlockConstraint::is_valid () const
	{
		return true;
	}

	BlockConstraint::LengthType
	BlockConstraint::block_width () const
	{
		return _block_width;
	}

	BlockConstraint::LengthType
	BlockConstraint::block_height () const
	{
		return _block_height;
	}

} // namespace BS
