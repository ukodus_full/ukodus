#include "instance.hpp"

namespace BS
{

	InstanceParms::InstanceParms (RawEntryValue num_allowed_values_)
	:
		_num_allowed_values(num_allowed_values_),
		_constraints()
	{}

	void
	InstanceParms::noreturn_register_constraint (std::unique_ptr<Constraint const> new_constraint)
	{
		assert(new_constraint->num_colors() == _num_allowed_values);
		assert(new_constraint->is_valid());

		_constraints.emplace_back(std::move(new_constraint));
	}

	InstanceParms &
	InstanceParms::register_constraint (std::unique_ptr<Constraint const> new_constraint) &
	{
		noreturn_register_constraint(std::move(new_constraint));
		return *this;
	}

	InstanceParms &&
	InstanceParms::register_constraint (std::unique_ptr<Constraint const> new_constraint) &&
	{
		noreturn_register_constraint(std::move(new_constraint));
		return std::move(*this);
	}

	Instance::Instance (InstanceParms parms)
	:
		_board(parms._num_allowed_values),
		_constraints(std::move(parms._constraints))
	{}

	Instance::Instance (Instance && other)
	:
		_board(std::move(other._board)),
		_constraints(std::move(other._constraints))
	{}

	Board const &
	Instance::as_board () const
	{
		return board();
	}

	EntryValue
	Instance::value (EntryIndex idx) const
	{
		return board().entry(idx).value;
	}

	bool
	Instance::state_is_valid () const
	{
		UT::RetCode const rc = for_each_constraint(
			[this] (Constraint const & constraint) -> UT::RetCode
			{
				return UT::success_if(board().satisfies(constraint));
			}
		);

		return (rc == UT::RetCode::success);
	}

	bool
	Instance::is_solved () const
	{
		return board().all_entries_are_set() and state_is_valid();
	}

	Board const &
	Instance::board () const
	{
		return _board;
	}

	Board &
	Instance::board ()
	{
		return _board;
	}

	void
	output (UT::OStream & out, Instance const & instance)
	{
		out << "Board:\n\n" << instance.as_board();
		out << "\nConstraints:" << std::endl;

		instance.for_each_constraint(
			[&out] (Constraint const & constraint) -> UT::RetCode
			{
				out << '\n' << constraint;
				return UT::RetCode::success;
			}
		);
	}

} // namespace BS
