#ifndef BASE_INSTANCE_HPP
#define BASE_INSTANCE_HPP

#include <utils/retcode.hpp>

#include <base/board.hpp>
#include <base/constraint.hpp>

#include <vector>
#include <memory>

namespace BS
{

	class InstanceParms final
	{
		friend class Instance;
	public:

		InstanceParms (RawEntryValue num_allowed_values_);

		InstanceParms &
		register_constraint (std::unique_ptr<Constraint const> new_constraint) &;

		InstanceParms &&
		register_constraint (std::unique_ptr<Constraint const> new_constraint) &&;

	private:
		void
		noreturn_register_constraint (std::unique_ptr<Constraint const> new_constraint);

		RawEntryValue const _num_allowed_values;
		std::vector<std::unique_ptr<Constraint const>> _constraints;
	};

	class Instance final
	{
	public:

		Instance (InstanceParms parms);

		Instance (Instance && other);

		// Forbid copying Instances
		Instance (Instance const &) = delete;

		// These are here just to adhere to the rule of 5
		Instance & operator= (Instance const &) = delete;
		Instance & operator= (Instance &&) = delete;
		~Instance () = default;

		Board const &
		as_board () const;

		EntryValue
		value (EntryIndex idx) const;

		bool
		state_is_valid () const;

		bool
		is_solved () const;

		// Applier should return a UT::RetCode from operator() (Constraint const &)
		template <typename Applier>
		UT::RetCode
		for_each_constraint (Applier applier) const;

	private:
		template <EntryModifier modifier_kind>
		friend class Modifier;

		Board const &
		board () const;

		Board &
		board ();

		/**
			Prints the instance's board at its current state plus all registered constraints.
			To print only the board use @c as_board().
		*/
		friend
		void
		output (UT::OStream & out, Instance const & instance);

		Board _board;
		std::vector<std::unique_ptr<Constraint const>> _constraints;
	};



	//BEGIN: Template implementation
	template <typename Applier>
	UT::RetCode
	Instance::for_each_constraint (Applier applier) const
	{
		for (auto const & uptr : _constraints)
		{
			UT::RetCode const rc = applier(*uptr);
			if (rc == UT::RetCode::failure) { return UT::RetCode::failure; }
		}

		return UT::RetCode::success;
	}
	//END: Template implementation

} // namespace BS

#endif /* BASE_INSTANCE_HPP */
