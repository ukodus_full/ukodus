#include "board.hpp"

#include <utils/grid_printer.hpp>

#include <base/constraint.hpp>
#include <base/constraint_check.hpp>

namespace BS
{

	Board::Board (RawEntryValue num_allowed_values_)
	:
		_num_allowed_values(num_allowed_values_),
		_entries(num_allowed_values() * num_allowed_values())
	{}

	Board::Board (Board && other)
	:
		_num_allowed_values(other._num_allowed_values),
		_entries(std::move(other._entries))
	{}

	RawEntryValue
	Board::num_allowed_values () const
	{
		return _num_allowed_values;
	}

	EntryIndex
	Board::num_entries () const
	{
		return _entries.size();
	}

	Entry const &
	Board::entry (EntryIndex idx) const
	{
		assert(idx < num_entries());

		return _entries[idx];
	}

	void
	Board::unchecked_set_value (EntryIndex idx, EntryValue new_value, EntryModifier new_modifier)
	{
		assert(idx < num_entries());
		assert((new_value == EntryValue::unset) or (value_of(new_value) < num_allowed_values()));
		assert(new_modifier != EntryModifier::none);

		Entry & the_entry = _entries[idx];

		assert(has_permission(new_modifier, the_entry.modifier));

		the_entry.value = new_value;

		if (new_value == EntryValue::unset)
		{
			the_entry.modifier = EntryModifier::none;
		}
		else
		{
			the_entry.modifier = new_modifier;
		}
	}

	bool
	Board::has_permission (EntryModifier new_modifier, EntryModifier last_modifier)
	{
		return (last_modifier == EntryModifier::none) or (new_modifier == last_modifier);
	}

	bool
	Board::all_entries_are_set () const
	{
		for (auto const & e : _entries)
		{
			if (e.value == EntryValue::unset) { return false; }
		}

		return true;
	}

	bool
	Board::satisfies (Constraint const & constraint) const
	{
		ColorUse color_use(constraint);
		return (color_use.fill(*this) == UT::RetCode::success);
	}

	void
	output (UT::OStream & out, Board const & board)
	{
		UT::GridPrinter(board.num_allowed_values()).print(out, board._entries.cbegin(), board._entries.cend());
	}

} // namespace BS
