#ifndef BASE_BASE_DEFINITIONS_HPP
#define BASE_BASE_DEFINITIONS_HPP

#include <utils/stream_wrapper.hpp>

#include <base/base_includes.hpp>

namespace BS
{

	using
	RawEntryValue = unsigned short;

	enum class EntryValue : RawEntryValue
	{
		unset = std::numeric_limits<RawEntryValue>::max()
	};

	constexpr inline
	EntryValue
	entry_from (RawEntryValue raw)
	{ 
		return static_cast<EntryValue>(raw);
	}

	constexpr inline
	RawEntryValue
	value_of (EntryValue entry)
	{
		return static_cast<RawEntryValue>(entry);
	}

	enum class EntryModifier
	{
		setter,
		player,
		solver,
		none
	};

	struct Entry final
	{
		Entry ()
		:
			value(EntryValue::unset),
			modifier(EntryModifier::none)
		{}

		EntryValue value;
		EntryModifier modifier;
	};

	using
	EntryIndex = std::size_t;

	/** @throws std::invalid_argument if the value is too high (and not @c EntryValue::unset). */
	void
	output (UT::OStream & out, EntryValue value);

	void
	output (UT::OStream & out, Entry entry);

} // namespace BS

#endif /* BASE_BASE_DEFINITIONS_HPP */
