#ifndef BASE_ARBITRARY_CONSTRAINT_HPP
#define BASE_ARBITRARY_CONSTRAINT_HPP

#include <base/constraint.hpp>

#include <vector>

namespace BS
{

	class ArbitraryConstraint final
	: public Constraint
	{
	public:

		ArbitraryConstraint (NumColorsType num_colors_);

		void
		set_color (EntryIndex idx, EntryValue color);

		EntryValue
		color (EntryIndex idx) const override;

	private:
		std::vector<EntryValue> _colors;
	};

} // namespace BS

#endif /* BASE_ARBITRARY_CONSTRAINT_HPP */
