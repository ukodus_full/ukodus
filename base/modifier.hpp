#ifndef BASE_MODIFIER_HPP
#define BASE_MODIFIER_HPP

#include <base/base_definitions.hpp>
#include <base/instance.hpp>

namespace BS
{

	template <EntryModifier modifier_type>
	class Modifier final
	{
	public:

		Modifier (Instance & instance_)
		: _instance(instance_) {}

		bool
		is_allowed_to_modify (EntryIndex idx) const
		{
			return Board::has_permission(modifier_type, instance().as_board().entry(idx).modifier);
		}

		void
		set_value (EntryIndex idx, EntryValue value) const
		{
			instance().board().unchecked_set_value(idx, value, modifier_type);
		}

	private:
		Instance &
		instance () const
		{
			return _instance;
		}

		Instance & _instance;
	};

	using Setter = Modifier<EntryModifier::setter>;
	using Player = Modifier<EntryModifier::player>;
	using Solver = Modifier<EntryModifier::solver>;

} // namespace BS

#endif /* BASE_MODIFIER_HPP */
