#ifndef BASE_BASE_INCLUDES_HPP
#define BASE_BASE_INCLUDES_HPP

/**
	@file base_includes.hpp

	Small and harmless #include's which should be available throughout the project.
*/

#include <cassert>
#include <limits>

#endif /* BASE_BASE_INCLUDES_HPP */
