#ifndef BASE_CONSTRAINT_HPP
#define BASE_CONSTRAINT_HPP

#include <base/base_definitions.hpp>

namespace BS
{

	/**
		@class Constraint

		Base class for all constraint variants.
	*/
	class Constraint
	{
	public:

		using
		NumColorsType = RawEntryValue;

		Constraint (NumColorsType num_colors_);

		virtual
		~Constraint () = default;

		NumColorsType
		num_colors () const;

		virtual
		EntryValue
		color (EntryIndex idx) const = 0;

		/**
			A @c Constraint is in a valid state if (1) there are no @c unset colors
			and (2) every color from 0 to <tt>num_colors() - 1</tt> is used precisely
			<tt>num_colors()</tt> many times.

			This base class offers a correct implementation but as a virtual method.
			The base implementation takes O(num_colors()^2) time, but some particular
			types of constraint might be able to check validity faster, and some may
			even be always valid by definition.
		*/
		virtual
		bool
		is_valid () const;

		EntryIndex
		num_entries () const;

	private:
		friend
		void
		output (UT::OStream & out, Constraint const & constraint);

		NumColorsType const _num_colors;
	};

} // namespace BS

#endif /* BASE_CONSTRAINT_HPP */
