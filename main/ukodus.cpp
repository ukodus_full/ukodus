#include <utils/stream_wrapper.hpp>
#include <utils/time_printer.hpp>

#include <base/instance.hpp>

#include <interface/text_file_setter.hpp>

#include <algorithms/algorithms.hpp>

#include <stdexcept>

int main (int argc, char ** argv)
{
	if (argc != 2)
	{
		throw std::invalid_argument("Ukodus requires precisely 1 argument from the command line.");
	}

	UT::OStream & out = UT::coutw();

	BS::Instance instance = IF::TextFileParser::new_instance(argv[1]);

	out
		<< "Initial state:" << std::endl
		<< instance.as_board() << std::endl
		<< "State is valid: " << std::boolalpha << instance.state_is_valid() << std::endl;

	auto const result = AL::Algorithms::default_try_solve(instance);

	out
		<< "Trying to solve the instance with default algorithm: "	<< result.code << std::endl
		<< "Took: " << UT::PrettyTime(result.time) << std::endl
		<< std::endl
		<< "Final state:" << std::endl
		<< instance.as_board();
}
