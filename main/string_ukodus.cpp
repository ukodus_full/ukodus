#include <utils/time_printer.hpp>

#include <interface/single_string_solver.hpp>

int main (int argc, char ** argv)
{
	if (argc != 2)
	{
		UT::coutw() << argc << std::endl;
		UT::coutw() << argv[1] << std::endl;
		throw std::invalid_argument("String-Ukodus requires precisely 1 argument from the command line.");
	}

	UT::OStream & out = UT::coutw();

	auto const result = IF::StringSolver::parse_and_try_solve(argv[1]);

	out
		<< "Trying to solve the instance with default algorithm: "	<< result.first.code << std::endl
		<< "Took: " << UT::PrettyTime(result.first.time) << std::endl
		<< "Final state: " << result.second << std::endl;
}
